extends Control

var mouse_position : Vector2 = Vector2.ZERO

export(bool) var centered:bool = true
export(Vector2) var v_size:Vector2 = Vector2(480, 360)
export(bool) var y_bottom_up = true

func _gui_input(event):
	if event is InputEventMouseMotion:
		mouse_position = event.position
		update()

func _draw():
	draw_line(Vector2(0, mouse_position.y), 
		Vector2(rect_size.x, mouse_position.y), Color(0, 0, 0, .5))

	draw_line(Vector2(mouse_position.x, 0), 
		Vector2(mouse_position.x, rect_size.y), Color(0, 0, 0, .5))
		
func l2v(local):
	var virtual = local * v_size / rect_size
	
	if centered:
		virtual -= v_size / 2
		
	if y_bottom_up:
		virtual.y = v_size.y - virtual.y
		
	return virtual

func virtual_mouse_position():
	var virtual = get_local_mouse_position() * v_size / rect_size
	
	if y_bottom_up:
		virtual.y = v_size.y - virtual.y

	if centered:
		virtual -= v_size / 2
		
	return virtual
