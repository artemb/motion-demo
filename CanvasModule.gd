extends Control

onready var xLabel : Label = find_node('xLabel')
onready var yLabel : Label = find_node('yLabel')
onready var canvas : Panel = find_node('Canvas')

func _on_Canvas_gui_input(event):
	if event is InputEventMouseMotion:
		update_coordinate_labels()
		
func update_coordinate_labels():
	xLabel.set_global_position(Vector2(get_global_mouse_position().x - xLabel.rect_size.x / 2, 
		xLabel.rect_global_position.y))

	xLabel.text = str(int(canvas.virtual_mouse_position().x))

	yLabel.set_global_position(Vector2(yLabel.rect_global_position.x, get_global_mouse_position().y - xLabel.rect_size.y / 2))
	yLabel.text = str(int(canvas.virtual_mouse_position().y))
